/*
    créer une classe mère `Animal` qui prend en compte les caractéristiques propres aux animaux en tout genre, 
    puis créer des classes filles d'animaux spécifiques à loisir, en leur attribuant les spécificités dues a leur famille 
    (une classe `Oiseau` aura une propriété envergure et un propriété type de bec, mais pas un chat.. etc)
 */

// cet objet appelé énumération permet d'accéder à des valeurs fixes de façon pratique et lisible
// utile lorsqu'une variable peut avoir un nombre limité de valeurs possibles 
const animal_classe = {
    AVES: "Oiseau",
    MAMMALIA: "Mammifere",
    REPTILIA: "Reptile"
}
const animal_genre = {
    M: "mâle",
    F: "femelle",
    N: undefined
}

class Animal {
    constructor(classe, nom, age, genre) {
        this.classe = classe;
        this.nom = nom;
        this.age = age;
        this.genre = genre;
    }

    //CLASSE renvoie à notre énumération animal_classe et permet d'accéder facilement a la liste des classes d'animaux
    static get CLASSE() {
        return animal_classe;
    }

    //même chose pour GENRE
    static get GENRE() {
        return animal_genre;
    }

    //renvoie une liste de clés:valeurs representant les propriétés de notre animal
    proprietes() {
        let proprietes = "";
        for (const [cle, val] of Object.entries(this)) {
            proprietes += `${cle} : ${val}\n`
        }
        return proprietes;
    }

    description() {
        return `${this.nom}, ${this.classe} ${this.genre} âgé${this.genre === Animal.GENRE.F ? "e" : ''} de ${this.age} mois.`;
    }
    //conditions ternaires
    //permet de tester une condition sur une ligne et donc de l'utiliser dans nos chaines
    //syntaxe : condition ? positif : negatif
    //exemple if i != 0 ? i : 0 veut dire si i différent de 0 afficher i, sinon afficher 0

}

//notre classe Oiseau hérite d'Animal
class Oiseau extends Animal {
    constructor(nom, age, genre, espece, envergure, bec, cri) {
        //on appelle le constructeur d'Animal pour affecter les propriétés propres à l'Animal
        //un Oiseau fait toujours partie de la classe Aves donc pas besoin de préciser la classe dans le constructeur
        //cependant le constructeur d'Animal nécessite une classe, nous lui passons ce paramètre au travers de super
        super(Animal.CLASSE.AVES, nom, age, genre);
        //on affecte les propriétés propre à Oiseau normalement
        this.espece = espece;
        this.envergure = envergure;
        this.bec = bec;
        this.cri = cri;
    }

    //un oiseau peut crier
    crier() {
        return `${this.nom} le ${this.espece} crie : "${this.cri}" !`;
    }

    //on récupère la description initiale et ajoute les spécificités de l'Oiseau
    description() {
        return super.description() + `\nC'est un${this.genre === Animal.GENRE.F ? "e" : ''} ${this.espece} d'une envergure de ${this.envergure}cm.\nSon cri ressemble à "${this.cri}"`;
    }
}

//notre classe Chien hérite également d'Animal
class Chien extends Animal {
    constructor(nom, age, genre, race, taille, robe) {
        super(Animal.CLASSE.MAMMALIA, nom, age, genre);
        this.race = race;
        this.taille = taille;
        this.robe = robe;
    }
    aboiement() {
        return `${this.nom} aboie! `;
    }
    description() {
        return super.description() + `\nC'est un${this.genre === Animal.GENRE.F ? "e" : ''} chien${this.genre === Animal.GENRE.F ? "ne" : ''}, ${this.race} à la robe ${this.robe} qui mesure ${this.taille}cm`;
    }
}


//un specimen mammifère mâle, avec un age de 8 mois
//pour définir le genre, on utilise notre énumération via Animal.GENRE
const specimen1 = new Animal(Animal.CLASSE.MAMMALIA, "Joe", 8, Animal.GENRE.M);
//on affiche ses proprietes dans la console
console.log(specimen1.proprietes());
//puis sa description
console.log(specimen1.description());


//on crée une sittelle corse (sitta whiteheadi) nommée Roger, agée de 13 mois d'une envergure de 21cm
//son bec est fin et son chant fait "pupupupupu"
const oiseau1 = new Oiseau("Roger", 13, Animal.GENRE.M, "Sitta whiteheadi", 21, "fin", "pupupupupu");
console.log(oiseau1.proprietes());
console.log(oiseau1.description());
console.log(oiseau1.crier());

//on crée une canne colvert (Anas platyrhynchos) nommée Michelle, agée de 16 mois d'une envergure de 89cm
//son bec est fin et son cri fait "coin coin"
//Ici on peut également utiliser Oiseau pour obtenir la valeur F (femelle), puisque Oiseau hérite de Animal
const oiseau2 = new Oiseau("Michelle", 16, Oiseau.GENRE.F, "Anas platyrhynchos", 89, "plat", "coin coin");
console.log(oiseau2.proprietes());
console.log(oiseau2.description());
console.log(oiseau2.crier());


//on crée une shiba nommée Gally âgée de 18 mois, de 38cm de hauteur avec une robe sésame
const chien1 = new Chien("Gally", 18, Chien.GENRE.F, "Shiba", 38, "sésame");
console.log(chien1.aboiement());
console.log(chien1.proprietes());
console.log(chien1.description());
