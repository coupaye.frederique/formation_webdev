function clic(){
    console.log("clic");
}

let el_bouton1 = document.getElementById("bouton1");

//Ajout d'un gestionnaire pour l'évènement click
el_bouton1.addEventListener("click", clic);
//retrait d'un gestionnaire pour l'évènement click
el_bouton1.removeEventListener("click", clic);

//les familles d'evenements
//evenements souris ex: clic, relacher le clic, survol d'une zone..
//evenements clavier ex: appui d'une touche, relachement d'une touche...
//evenements fenêtre ex: changer de page, scroller, fermeture de la page...
//evenements de formulaires ex: changement de champ de saisie, envoi d'un formulaire...

//ajout d'un gestionnaire qui affiche le type et la cible de l'évènement
el_bouton1.addEventListener("click", function(event){
    console.log(`Event : ${event.type}, texte de la cible ${event.target.textContent}`);
    event.target.style.color = "blue";
});

document.addEventListener("keypress", function(event){
    console.log(event)
    //un evenement clavier possède un charCode
    //ce charCode est l'identifiant de la touche concernée par l'évènement
    console.log(`Appui sur la touche ${String.fromCharCode(event.which)}`)
});


//l'evenement load s'active au moment du chargement de la page
window.addEventListener("load", function(){
    console.log("page chargée");
});
/*
//l'evenement beforeunload s'active au moment du déchargement de la page
window.addEventListener("beforeunload", function(event){
    let message = "Reste !";
    event.returnValue = message;
    return message;
})
*/

document.addEventListener("click", function(event){
    console.log(event);
    //event.button recupere le type de clic pour un evenement click
    //0 represente le clic gauche, 1 le clic du milieu, 2 le clic droit
    console.log(`type de clic : ${event.button}`);
    //event.clientX et event.clientY contiennent respectivement la position 
    //du curseur en x et en y sur la page
    console.log(`position x: ${event.clientX} position y: ${event.clientY}`);
    console.log("Evenement de clic sur le document")
});

document.getElementById("para").addEventListener("click", function(event){
    console.log("Evenement de clic sur le paragraphe");
    event.stopPropagation()
});

document.getElementById("propa").addEventListener("click", function(event){
    console.log("Evenement de clic sur le bouton");
    //arrête la propagation d'évènement
    event.stopPropagation();
});

document.getElementById("bloque").addEventListener("click", function(event){
    console.log("lien bloqué");
    event.preventDefault();
});