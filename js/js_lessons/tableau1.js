
function lister_elements(tableau) {
    console.log("tableau:");
    for (let i = 0; i < tableau.length; i++){
        console.log(`${i} : ${tableau[i]}`);
    }
}

const liste_courses = ["Ravioli", "Eponge", "Mouchoirs"];

console.log(liste_courses.length)

//      0        1          2
// | RAVIOLI | EPONGE | MOUCHOIRS |
console.log("Appeler les cases une à une")
console.log(liste_courses[0]); //ravioli
console.log(liste_courses[1]); //eponge
console.log(liste_courses[2]); //mouchoirs

console.log("Utiliser un for");
for (let i = 0; i < liste_courses.length; i++){
    console.log(liste_courses[i]);
}

console.log("Utiliser un forEach");
liste_courses.forEach( course => {
    console.log(course);
});

console.log("Utiliser un for of");
for (const course of liste_courses){
    console.log(course);
}

//ajouter un élément à la fin du tableau
liste_courses.push("Sauce tomate");
lister_elements(liste_courses);

//ajouter un élément au début de notre tableau
liste_courses.unshift("Piles");
lister_elements(liste_courses);

//supprimer dernier élément du tableau
console.log("On supprime " + liste_courses.pop());
lister_elements(liste_courses);

//splice (raccord) permet de supprimer et remplacer des éléments
//le premier argument correspond a la case de départ de notre raccord
//le deuxième argument correspond au nombre d'éléments à supprimer de notre tableau
//les arguments suivants sont les éléments que l'on veut ajouter 
//à partir de la case de départ
liste_courses.splice(0, 1, "Bouteille de whisky", "Verres");
lister_elements(liste_courses);

//slice (tranche) récupère une partie d'un tableau 
//en faisant une copie partant d'une borne inférieure incluse
//et d'une borne supérieure exclue 
const liste_courses_essentielles = liste_courses.slice(0,2);
lister_elements(liste_courses_essentielles);


//EXERCICE 
//Les trois mousquetaires
//Créez un tableau mousquetaires
//contenant les noms des trois mousquetaires
//Athos, Porthos, Aramis
//Affichez le nom de chaque mousquetaire
//Ajoutez d'Artagnan au tableau
//Affichez le nom de tous les mousquetaires 
//à l'aide de deux méthodes différentes
//-ex: for, forEach, for of...

//Avant votre dernière boucle
//Supprimer Porthos du tableau 
//en conservant d'artagnan

const mousquetaires = ["Athos", "Porthos", "Aramis"];

console.log(mousquetaires[0]); //Athos
console.log(mousquetaires[1]); //Porthos
console.log(mousquetaires[2]); //Aramis
console.log(mousquetaires); // ["Athos", "Porthos", "Aramis"]

mousquetaires.push("D'Artagnan");
//mousquetaires.unshift("D'Artagnan");

for (let i = 0; i < mousquetaires.length; i++){
    console.log(mousquetaires[i]);
}

for (const mousquetaire of mousquetaires){
    console.log(mousquetaire);
}

//Supprimer l'élément en case 1
mousquetaires.splice(1, 1);

mousquetaires.forEach( mousquetaire => {
    console.log(mousquetaire);
} );



//EXERCICE 
//Faire la somme des valeurs d'un tableau
//à l'aide d'une seule boucle
//stocker le résultat dans une variable 
//nommée somme
//i              0 1 2  3 4  5
const valeurs = [4,1,8,49,10,3];

let somme = 0;
for(let i = 0; i < valeurs.length; i++){
    somme += valeurs[i];
}
   

console.log(`La somme des valeurs est "${somme}"`);


//EXERCICE 
//Ecrire une fonction renvoyant 
//les n derniers éléments d'un tableau passé en paramètre
//Avec un deuxième paramètre, on pourra spécifier
//le nombre n d'éléments à renvoyer
let tab = [1, 5, 8, 1, 198, 15, -1, 9, -7]

function derniers_elements(tableau, n){
    for(let i = tableau.length - n; i < tableau.length; i++){
        console.log(tableau[i]);
    }
}
// en partant de la fin de la liste
function derniers_elements_inv(tableau, n){
    for(let i = tableau.length-1; i > tableau.length - n - 1; i+=-1){
        console.log(tableau[i]);
    }
}

//renvoie les 3 derniers éléménts du tableau tab
derniers_elements(tab, 3);
//renvoie les 3 derniers éléments du tableau tab en partant de la fin
derniers_elements_inv(tab, 3);

//EXERCICE 
//Ecrire une fonction renvoyant 
//les n premiers éléments d'un tableau passé en paramètre
//Avec un deuxième paramètre, on pourra spécifier
//le nombre n d'éléments à renvoyer

function premiers_elements(tableau, n){
    for (let i = 0; i < n; i++){
        console.log(tableau[i]);
    }
}

let tab_2 = [1, 5, 8, 1, 198, 15, -1, 9, -7]

premiers_elements(tab, 3);
premiers_elements(tab_2, 2);

//EXERCICE 
//Ecrire une fonction créant une phrase 
//A partir d'un tableau de mots
//ils devront être séparés par des espaces
let tab_3 = ["Salut","les","amis","!"];

function joindre_mots(tableau){
    let phrase = "";
    for (let i = 0; i < tableau.length; i++){
        //si on est pas sur la première case
        if (i != 0){
            //ajoute le mot a la case i à notre phrase
            //et le précède d'un espace
            phrase += " " + tableau[i];
        } else {
            //ajout ele mot a la case i à notre phrase
            phrase += tableau[i];
        }
    }
    /*
    tableau.forEach( mot => {
        if (tableau.indexOf(mot) === 0){
            phrase += mot;
        } else {
            phrase += " " + mot;
        }
    });
    */
    console.log(phrase);
}

joindre_mots(tab_3);

//EXERCICE
//Ecrire une fonction renvoyant le nombre le plus grand
//d'un tableau de valeurs
let tab_4 = [1,4,8,19,5,20,3,9,0,-1,-6,30,11,50,4,9];

//génère un tableau aléatoire de taille 5000 comprenant des nombres entre 0 et 500
let tab_alea = Array.from({length: 400}, () => Math.floor(Math.random() * 50000));

function valeur_max(tableau){
    let max = -Infinity;
    /*
    tableau.forEach( nombre => {
        if (nombre > max){
            max = nombre;
        }
    });
    */
   for (let i = 0; i < tableau.length; i++){
       if (tableau[i] > max){
           max = tableau[i];
        }
    }
    return max;
}

console.log(valeur_max(tab_4));
console.log(valeur_max(tab_alea));

function valeur_min(tableau){
    let min = Infinity;
    /*
    tableau.forEach( nombre => {
        if (nombre < min){
            min = nombre;
        }
    });
    */
   for (let i = 0; i < tableau.length; i++){
       if (tableau[i] < min){
           //then
           min = tableau[i];
        } 
    }
    return min;
}

console.log(valeur_min(tab_4));
console.log(valeur_min(tab_alea));

//Implementer l'algorithme de tri par selection
//  0 1 2 3  4 5  6 7 ....
// [1,4,8,19,5,20,3,9,0,-1,-6,30,11,50,4,9]

function tri_selection(tableau){
    for (let i = 0; i < tableau.length; i++){

        let i_min = i; 
        for (let j = i+1; j < tableau.length; j++){
            //si on trouve un element a la mauvaise place    
            if (tableau[j] < tableau[i_min]){
                //on le marque
                i_min = j;
            }
        }
        //si un element a été marqué
        if (i_min != i){
            //on permute les elements du tableaux
            //de façon a les mettre dans le bon ordre
            let element = tableau[i];
            tableau[i] = tableau[i_min];
            tableau[i_min] = element;
        }
    } 
}
console.log(tab_4);
tri_selection(tab_4);
console.log(tab_4);