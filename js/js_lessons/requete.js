/*//création d'une requête http
let requete = new XMLHttpRequest();

//on prépare une requête HTTP GET vers le fichier films.json
requete.open("GET", "http://localhost/~ziroshell/formation/js/js_lessons/films.json");


//gestion de la requête de façon asynchrone
//on attend que la requête soit chargée pour afficher son contenu
requete.addEventListener("load", function(){
    //si le code de retour est bon (compris entre [200,400[)
    if (requete.status >= 200 && requete.status < 400){
        //afficher la reponse de la requête
        console.log(requete.responseText);
    } else {
        //afficher l'erreur concernant notre requete
        console.error(`${requete.status} - ${requete.statusText}`);
    }
});

//error se déclenche lorsque le serveur est inatteignable
//et ne peut donc pas renvoyer notre requête 
requete.addEventListener("error", function(){
    //La requête n'a pas reussi a atteindre le serveur
    console.error("La requête n'a pas atteint le serveur");
})


//on envoie notre requête
requete.send();*/

//Execution d'un appel AJAX GET
//AJAX = Asynchronous Javascript And Xml 
//Les paramètres sont l'URL cible et la fonction callback appelée 
//en cas de succès de la requête
function ajaxGet(url, callback){
    //création d'une requête http
    let requete = new XMLHttpRequest();

    //on prépare une requête HTTP GET vers le fichier films.json
    requete.open("GET", url);
    //gestion de la requête de façon asynchrone
    requete.addEventListener("load", function(){
        //si le code de retour est bon (compris entre [200,400[)
        if (requete.status >= 200 && requete.status < 400){
            //Appel de la fonction callback en lui passant la réponse
            callback(requete.responseText);
        } else {
            //afficher l'erreur concernant notre requete
            console.error(`${requete.status} - ${requete.statusText}`);
        }
    });
    requete.addEventListener("error", function(){
        //La requête n'a pas reussi a atteindre le serveur
        console.error("La requête n'a pas atteint le serveur");
    })
    //on envoie notre requête
    requete.send();
}

ajaxGet("http://localhost/~ziroshell/formation/js/js_lessons/films.json", function(reponse){
    console.log(reponse);
    //récupère le contenu d'une réponse au format JSON 
    //et le place dans des structures de données javascript
    let tab_objet_reponse = JSON.parse(reponse);
    console.log(tab_objet_reponse);

    for (const objet_reponse of tab_objet_reponse){
        let el_option = document.createElement("option");
        el_option.textContent = objet_reponse.titre;
        document.getElementById("films").appendChild(el_option);
    }
});

