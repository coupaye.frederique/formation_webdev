function ajaxGet(url, callback){
    //Crée une requête HTTP
    var req = new XMLHttpRequest();

    //définit notre requête de type GET qui va requêter notre URL
    req.open("GET", url);

    //au moment ou la requête renvoie une réponse
    req.addEventListener("load", function(){
        //on vérifie qu'il n'y ait pas d'erreurs
        if (req.status >= 200 && req.status < 400) {

            //On appelle la fonction en callback en lui passant la réponse
            callback(req.response);

        } else {

            console.error(`${req.status} - ${req.statusText} - ${url}`);

        }

    });

    //l'event error correspond à un serveur injoignable
    req.addEventListener("error", function(){
        
        console.error(`Serveur injoignable à l'aide de l'url ${url}`);

    });

    //send ne prend de paramètre que si la requête est de type POST
    req.send();
}


ajaxGet("https://api-adresse.data.gouv.fr/search/?q=montpellier&type=street", function(response){
    let jsonResponse;
    jsonResponse = JSON.parse(response);
    console.log(jsonResponse);
})



