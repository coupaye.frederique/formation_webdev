<?php

//on précise le contexte du controlleur à Symfony
namespace App\Controller;

//boîte à outils concernant les requêtes HTTP
use Symfony\Component\HttpFoundation\Request;

//on précise qu'on souhaite utiliser les Annotations
//pour effectuer le routage de nos requêtes
use Symfony\Component\Routing\Annotation\Route;

//on précise l'utilisation de la classe abstraite AbstractController
//cette classe AbtractController contenue dans symfony permet l'utilisation
//de méthodes propres aux controllers 
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use App\Entity\Advert;


//pour que notre controller puisse utiliser les méthodes de AbstractController
//il faut que notre AdvertController hérite de AbstractController
/**
 * @Route("/advert")
 */
class AdvertController extends AbstractController
{

    /**
     * @Route("/{page}", name="advert_index", requirements={
     *  "page" = "\d+"
     * }, 
     * defaults={
     *  "page" = 1
     * })
     */
    public function index($page)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $repository = $this->getDoctrine()->getRepository(Advert::class);
        
        $listAdverts = $repository->findAll();
        
        $content = $this->render('Advert/index.html.twig', [
            'page' => $page,
            'listAdverts' => $listAdverts
        ]);
        return $content;
    }

    /**
     * @Route("/view/{id}", name="advert_view", requirements={
     *  "id" = "\d+"
     * })
     */
    public function view($id)
    {
        $repository = $this->getDoctrine()->getRepository(Advert::class);
        $advert = $repository->find($id);

        $content = $this->render('Advert/view.html.twig', [
            'advert' => $advert
        ]);

        return $content;
    }

    /**
     * @Route("/add", name="advert_add")
     */
    public function add(Request $request)
    {   
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $user = $this->getUser();

        //on prépare une instance de Advert pour y renseigner les données du formulaire
        $advert = new Advert();
        
        //on prépare les champs de notre formulaire à l'aide de formBuilder
        //on renseigne quelle Entité sera hydratée par notre formulaire grâce au paramètre de createFormBuilder 
        $form = $this->createFormBuilder($advert)
        ->add('title', TextType::class)
        ->add('content', TextareaType::class)
        ->add('image', FileType::class, ['label' => "Image (PNG, JPG)"])
        ->add('submit', SubmitType::class, ['label' => 'Poster Annonce'])
        ->getForm();  
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()){
            //récupération du fichier uploadé
            $file = $form->get('image')->getData();

            $advert->setAuthor($user->getUsername());
            $advert->setDate(new \DateTime()); //on précise la date d'envoi
            $advert->setUser($user);
            $advert = $form->getData(); //on hydrate notre instance d'Advert
            
            //génération d'un nom de fichier unique
            $unique = md5(uniqid());
            $filename = $unique.'.'.$file->guessExtension();

            try {
                //on déplace notre fichier dans notre dossier d'uploads
                //ce dossier d'uploads se paramètre dans config/service.yaml sous la mention parameters
                $file->move(
                    $this->getParameter('image_directory'),
                    $filename
                );
            } catch (FileException $e){
                $this->addFlash('info', 'Erreur envoi image');
                return $this->render('Advert/add.html.twig', [
                    'form' => $form->createView()
                ]);
            }

            $advert->setImage($filename);

            $this->addFlash('info', 'Annonce bien enregistrée');
            $entityManager = $this->getDoctrine()->getManager(); //On récupère le manager d'entités
            $entityManager->persist($advert); //on demande au manager d'envoyer l'entité dans la bdd
            $entityManager->flush(); //on indique la fin des opérations de bdd 



            return $this->redirectToRoute('advert_view', ['id' => $advert->getId()]);
        }

        return $this->render('Advert/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="advert_edit", requirements={
     *  "id" = "\d+" 
     * })
     */
    public function edit($id, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $user = $this->getUser();

        //on récupère une instance de Advert pour renseigner les données du formulaire
        $repository = $this->getDoctrine()->getRepository(Advert::class);
        $advert = $repository->find($id);

        if ($advert->getAuthor() != $user->getUsername()){
            return $this->redirectToRoute('advert_view', ['id' => $advert->getId()]);
        }

        //on prépare les champs de notre formulaire à l'aide de formBuilder
        //en passant notre entité récupérée à notre formulaire il se chargera de remplir les champs
        $form = $this->createFormBuilder($advert)
        ->add('title', TextType::class)
        ->add('content', TextareaType::class)
        ->add('submit', SubmitType::class, ['label' => 'Poster Annonce'])
        ->getForm();  

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()){
            $this->addFlash('info', 'Annonce bien enregistrée');
            
            $advert = $form->getData(); //on hydrate notre instance d'Advert

            $entityManager = $this->getDoctrine()->getManager(); //On récupère le manager d'entités
            $entityManager->persist($advert); // facultatif dans le cadre d'une édition
            $entityManager->flush(); //on indique la fin des opérations de bdd 

            return $this->redirectToRoute('advert_view', ['id' => $advert->getId()]);
        }
        
        return $this->render('Advert/edit.html.twig', [
            'advert' => $advert,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="advert_delete", requirements={
     *  "id" = "\d+"
     * })
     */
    public function delete($id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $user = $this->getUser();

        //récupération de l'advert dans notre bdd
        $repository = $this->getDoctrine()->getRepository(Advert::class);
        $advert = $repository->find($id);

        if ($advert->getAuthor() != $user->getUsername()){
            return $this->redirectToRoute('advert_view', ['id' => $advert->getId()]);
        }

        $entityManager = $this->getDoctrine()->getManager(); //On récupère le manager d'entités
        $entityManager->remove($advert);
        $entityManager->flush();

        $this->addFlash('info', 'Annonce bien supprimée');
        return $this->redirectToRoute('advert_index');
    }

    public function menu()
    {
        
        $repository = $this->getDoctrine()->getRepository(Advert::class);
        
        $listAdverts = $repository->findBy([], ['date' => 'DESC'], 3);

        return $this->render('Advert/_menu.html.twig', ['listAdverts' => $listAdverts]);
    }
}