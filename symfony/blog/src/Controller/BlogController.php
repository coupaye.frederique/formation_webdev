<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/", name="blog_index")
     */
    public function index()
    {
        //doctrine
        //requete (find findall)
        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
        ]);
    }

    /**
     * @Route("/view/{id}", name="blog_view")
     */
    public function view($id){

    }

    /**
     * @Route("/post", name="blog_post")
     */
    public function post(Request $request)
    {   
        $article = new Article();
        //on prépare les champs de notre formulaire à l'aide de formBuilder
        //on renseigne quelle Entité sera hydratée par notre formulaire grâce au paramètre de createFormBuilder 
        $form = $this->createFormBuilder($article)
        ->add('title', TextType::class)
        ->add('content', TextareaType::class)
        ->add('submit', SubmitType::class, ['label' => 'Poster Article'])
        ->getForm();  
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()){
            $this->addFlash('info', 'Annonce bien enregistrée');
            
            $author = new Author();
            $author->setName('Test');
            $author->setUsername('Test');
            $author->setPassword('test');
            $author->setEmail('test@test.fr');

            $article->setDate(new \DateTime()); //on précise la date d'envoi
            
            $article->setAuthor($author);
            $article = $form->getData(); //on hydrate notre instance d'article

            $entityManager = $this->getDoctrine()->getManager(); //On récupère le manager d'entités
            $entityManager->persist($article); //on demande au manager d'envoyer l'entité dans la bdd
            $entityManager->flush(); //on indique la fin des opérations de bdd 

            return $this->redirectToRoute('blog_view', ['id' => $article->getId()]);
        }

        return $this->render('Article/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
