<?php
//on importe nos fonctions
require('includes/bdd_functions.php');

//on se connecte a la bdd a l'aide de notre fonction bdd_connect();
try {
    $pdo = bdd_connect();
} catch (PDOException $exception) {
    die($exception);
}

//on vérifie les données envoyées par le formulaire
$verification = false;
if (!empty($_POST['title']) && !empty($_POST['content'])) {
    $verification = true;
} else {
    $error_message = "Information manquante, veuillez vérifier les champs du formulaire";
}

if ($verification) {
    //on récupère les données du formulaire pour les envoyer a la bdd
    $title = $_POST['title'];
    $content = $_POST['content'];
    $date_sent = time();

    //on crée notre article avec la fonction createArticle
    if (bdd_createArticle($pdo, $title, $content, $date_sent)){
        header('Location: index.php');
    } else {
        //si une erreur survient on redirige avec un message d'erreur
        $error_message = 'La création de l\'article a échoué';
        header('Location: article_edit.php?error_message=' . $error_message);
    }
} else {
    //si une erreur survient on redirige avec un message d'erreur
    header('Location: article_edit.php?error_message=' . $error_message);
}
