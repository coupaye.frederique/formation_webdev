<?php
$host = 'localhost';
$db = "test";
$user = "root";
$password = 'password';

//connexion a notre bdd
try {
    $pdo = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $password);
    
} catch (PDOException $exception) {
    die($exception);
}

//on utilise une requête SELECT pour trouver notre utilisateur
//on fait en sorte de le trouver soit via son pseudo soit via son email
$query = $pdo->prepare('SELECT id, username, password_hash FROM user WHERE username = :username OR email = :username');

//on execute la requête
    $username = $_POST['username'];
    $user_password = $_POST['password'];
    $query->execute(
        array(':username' => $username)
    );

//si on récupère une ligne de données c'est que l'utilisateur a été trouvé
if ($data = $query->fetch()){
    //une fois l'utilisateur trouvé, on peut comparer le mot de passe du formulaire
    //avec le mot de passe haché contenu dans notre bdd à l'aide de password_verify()
    if (password_verify($user_password, $data['password_hash'])){
        //si password_verify renvoie true alors notre utilisateur est connecté
        session_start(); //on commence une session pour l'utilisateur
        $_SESSION['user_id'] = $data['id']; //on stocke son id
        $_SESSION['username'] = $data['username']; //on stocke son pseudo
        $_SESSION['sign_in_time'] = time(); // on stocke l'heure de sa connexion
    } else {
        $error_message = 'Mot de passe incorrect';
    }
} else {
    //sinon on explique que l'utilisateur sous ce pseudo ou email est introuvable
    $error_message = 'Utilisateur introuvable';
}
//si aucune erreur n'a transparu
if (empty($error_message)){
    //on envoie vers la page de notre utilisateur
    header('Location: user_page.php');
} else {
    //sinon on renvoie vers la page de connexion accompagnée du message d'erreur
    header('Location: sign_in.php?error_message='.htmlspecialchars($error_message));
}

?>