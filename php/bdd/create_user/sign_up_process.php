<?php
$host = 'localhost';
$db = "test";
$user = "root";
$password = 'password';

//connexion a la base de données
try {
    $pdo = new PDO('mysql:host=' . $host . ';dbname=' . $db . ';charset=utf8', $user, $password);

} catch (PDOException $exception) {
    die($exception);
}

//préparation de notre requête INSERT INTO
//on précise les colonnes dont les valeurs seront renseignées
//puis après VALUES on précise dans un tableau les valeurs à renseigner
//ici dans prepare() on insère des noms de variables
$query = $pdo->prepare('INSERT INTO user (username, password_hash, email, sign_up_date)
VALUES (:username, :password_hash, :email, :sign_up_date)');

//à ce stade là nous devrions vérifier l'intégrité des données du formulaire
if (!empty($_POST['username']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password_repeat'])) {
    //on vérifie la longueur minimum du mot de passe
    if (mb_strlen($_POST['password']) >= 6) {
        //on vérifie l'égalité entre le mot de passe et la confirmation
        if ($_POST['password'] == $_POST['password_repeat']) {
            //verification de l'intégrité de l'email a l'aide de filter_var
            if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                //on vérifie que notre nom d'utilisateur corresponde à nos critères
                //ici l'expression régulière vérifie les critères suivants
                //premier caractère en minuscule, suivi de 4 à 30 caractères en min/maj alphanumériques et underscore acceptés
                //dernier caractère alphanumérique min/maj sans autres caractères acceptés
                if (preg_match('/[a-z]{1}[A-Za-z0-9\_]{4,30}[A-Za-z0-9]{1}/', $_POST['username'])) {
                    //on met true dans notre variable verification pour valider que tout est ok
                    $verification = true;
                } else {
                    //sinon on envoie un message d'erreur concernant le pseudo non conforme
                    $error_message = "Le pseudo doit faire entre 6 et 32 caractères, commencer par une minuscule et contenir des chiffres et des lettres ou des _";
                }

            } else {
                //sinon on envoie un message d'erreur concernant l'adresse mail non conforme
                $error_message = "Vous devez utiliser une adresse mail valide";
            }

        } else {
            //mots de passe non égaux
            $error_message = "mots de passe non égaux";
        }

    } else {
        //mot de passe trop court
        $error_message = "mot de passe trop court, 6 caractères minimum";
    }
} else {
    //informatoin manquante
    $error_message = "information manquante, veuillez vérifier votre formulaire";
}


//enfin, si on a aucun message d'erreur on effectue le traitement en base de données et redirige vers une confirmation
if ($verification) {
    //une fois les vérifications faites on peut récupérer les données du formulaire
    //et les stocker dans des variables et/ou les traiter
    $username = $_POST['username'];
    $email = $_POST['email'];
    //on hash le mot de passe avec la fonction pasword_hash($mot_de_passe, $algorithme)
    //l'utilisation recommandée est de juste préciser le mot de passe et l'algo par défaut
    $password_hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
    //on récupère une date d'inscription sous la forme d'une timestamp à l'aide de time()
    $sign_up_date = time();
    //puis on execute() notre $query en précisant quelles sont les valeurs de chaque variable
    $query->execute(
        array(':username' => $username,
            ':password_hash' => $password_hash,
            ':email' => $email,
            ':sign_up_date' => $sign_up_date)
    ); //ici au lieu d'utiliser bindparam on peut aussi utiliser un tableau associatif
    //que l'on passe à execute

    /*
    $query_error_code = $query->errorCode();//on récupère le code d'erreur après l'execution
    if ($query_error_code != "00000"){ //00000 signifie aucun problème, si différent signifie erreur
    if ($query_error_code == "23000"){ //23000 erreur de contrainte sur une requête
    $error_message = "Un utilisateur existe déjà avec ce pseudo et/ou email";
    }
    }
     */

    //on récupère errorInfo pour vérifier l'existence d'un problème lors d'execute()
    $query_error = $query->errorInfo(); //errorInfo() renvoie un tableau à 3 cases
    //la première case contient le code SQLSTATE sous la forme d'un code alphanumérique de 5 caractères
    //la deuxième case contient le code d'erreur du moteur SQL
    //la troisième case contient le message d'erreur du moteur en anglais
    //si tout se passe bien, le tableau aura la forme suivante : array('00000' , , )
    //si une erreur survient (duplicata par exemple) : array('23000', 1062, messagederreur);
    //si une erreur est survenue (si le code SQLSTATE n'est pas 00000)
    if ($query_error[0] != "00000") {
        //Si ce code d'erreur est celui du duplicata
        if ($query_error[1] == 1062) {
            //on renvoie un message approprié
            $error_message = "Un utilisateur existe déjà avec ce pseudo ou email";
        } else {
            //sinon on plante la page en envoyant le message d'erreur
            die($query_error[2]);
        }
    }
    header('Location: confirmation.php');
} else {
    //sinon on renvoie a la page de connexion avec un message d'erreur
    header('Location: sign_up.php?error_message=' . $error_message);
}
