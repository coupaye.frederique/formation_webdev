<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Recherche Apparts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
    <form method="post">
        <select name="column">
            <option value="*">Tout</option>
            <option value="id">Identifiant</option>
            <option value="city">Ville</option>
            <option value="address">Adresse</option>
            <option value="zip">Code postal</option>
            <option value="surface">Surface</option>
            <option value="rooms">Nombre de Pièces</option>
            <option value="rent">Loyer</option>
        </select>
        <input type="submit" value="Envoyer">
    </form>

    <?php
        try {
            $bdd_connection = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', 'password');
        } catch (PDOException $exception){
            die($exception->getMessage());
        }
        if (isset($_POST['column'])){
            $colonne = $_POST['column'];
            $query = $bdd_connection->query('SELECT ' . $colonne .  ' FROM appart');

            while ($data = $query->fetch()){
                print_r($data);
            }
        }
    
    ?>
</body>
</html>
