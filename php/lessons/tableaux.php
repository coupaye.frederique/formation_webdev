<?php
    //en appelant array on crée un tableau contenant les valeurs
    //passées en paramètre
    $prenoms = array('Nicolas', 'Quentin', 'Elsa', 'Jérôme dit "La Pause"');

    echo $prenoms[0]."</br>";
    echo $prenoms[1]."</br>";
    echo $prenoms[2]."</br>";
    echo $prenoms[3]."</br>";

    //si on précise un index l'élément sera ajouté a la case indiquée
    $prenoms_bis[0] = 'Nicolas';
    $prenoms_bis[1] = 'Quentin';
    $prenoms_bis[] = "Jérôme"; //Jérôme arrive après Quentin
    $prenoms_bis[2] = 'Elsa'; //ici Elsa prend la place de Jérôme qui disparait

    echo $prenoms_bis[0]."</br>";
    echo $prenoms_bis[1]."</br>";
    echo $prenoms_bis[2]."</br>";

    //si on ne précise pas d'index PHP ajoute les éléments a la suite
    $prenoms_ter[] = 'Nicolas';
    $prenoms_ter[] = 'Quentin';
    $prenoms_ter[] = 'Elsa';

    echo $prenoms_ter[0]."</br>";
    echo $prenoms_ter[1]."</br>";
    echo $prenoms_ter[2]."</br>";

    //tableaux associatifs
    $utilisateur  = array(
        'nom' => 'Muzy',
        'prenom' => 'Thomas',
        'adresse' => 'Pascialella',
        'ville' => 'Ste Lucie de Porto-Vecchio'
    );
    echo "</br>";
    echo $utilisateur['nom']." ".$utilisateur['prenom'];
    echo "</br>";
    echo $utilisateur['adresse'];
    echo "</br>";


    for ($i = 0; $i < 4; $i++){
        echo $prenoms[$i] . '</br>';
    }

    foreach($prenoms as $prenom){
        echo $prenom . '</br>';
    }

    foreach ($utilisateur as $propriete) {
        echo $propriete . '</br>';
    }

    foreach ($utilisateur as $key => $value) {
        echo $key. "=" .$value . '</br>';
    }

    print_r($utilisateur);

    //vérifier qu'une clé de tableau associatif existe
    $cle = "name";
    //array_key_exists(cle, tableau) renvoie true si cle existe dans tableau
    if (array_key_exists($cle, $utilisateur)){
        echo $utilisateur[$cle];
    } else {
        echo '</br>Cette clé n\'existe pas</br>';
    }
    //vérifier la présence d'une valeur dans un tableau
    $fruits = array('Banane', 'Pomme', 'Poire', 'Cerise', 'Framboise', 'Fraise');

    $fruit_a_chercher = 'Poire';
    //in_array(valeur, tableau) renvoie true si valeur existe dans tableau
    if (in_array($fruit_a_chercher, $fruits)){
        echo $fruit_a_chercher . ' est bien dans le tableau de fruits</br>';
    } else {
        echo $fruit_a_chercher . ' n\'est pas dans le tableau de fruits</br>';
    }
    //récupérer l'index d'une valeur dans un tableau
    $pos_fruits = array_search($fruit_a_chercher, $fruits);
    echo $fruit_a_chercher . ' est à l\'indice ' . $pos_fruits;


    //récuperer la longueur d'un tableau
    echo '</br>'.count($fruits);

    //récuperer la longueur d'une chaine
    echo mb_strlen('coucoü');
?>